﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RoshAlo.Resources;
using RoshAlo.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.IO.IsolatedStorage;
using RoshAlo.ViewModel;

namespace RoshAlo
{
    public partial class MainPage : PhoneApplicationPage
    {
        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            this.Loaded += MainPage_Loaded;

            Messenger.Default.Register<Uri>(this, "Navigate", (uri) => NavigationService.Navigate(uri));
        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingData);
            if (FeedList.Items.Count > 0)
                ProgressIndicatorHelper.HideProgressIndicator();
            base.OnNavigatedTo(e);
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if ((App.Current.Resources["Locator"] as ViewModelLocator).Main.BusyIndicatorVisibility)
            {
                Downloader.CancelStuffFromWeb();
                e.Cancel = true;
            }
            else
            {

                SaveData();

            }
            base.OnBackKeyPress(e);
        }

        private void SaveData()
        {
            if (_settings.Contains("lfeed"))
            {
                _settings["lfeed"] = (App.Current.Resources["Locator"] as ViewModelLocator).Main.AllNewsItem;
            }
            else
            {
                _settings.Add("lfeed", (App.Current.Resources["Locator"] as ViewModelLocator).Main.AllNewsItem);
            }

            _settings.Save();
        }
        
    }
}