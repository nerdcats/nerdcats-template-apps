﻿using GalaSoft.MvvmLight;
using HtmlAgilityPack;
using Microsoft.Phone.Shell;
using RoshAlo.Helpers;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RoshAlo.Model
{
    public class NewsItem : ObservableObject
    {
        public NewsItem()
        {
            details = new List<string>();
        }

        public string title { get; set; }
        public string imagrUrl { get; set; }
        public List<string> details { get; set; }
        public string summary { get; set; }
        public string date { get; set; }
        public string link { get; set; }
        public string author { get; set; }
    }

    public class NewsItemCollection : ObservableObject
    {
       

        private static NewsItemCollection _Instance;
        public static NewsItemCollection Instance 
        {
            get { return _Instance ?? (_Instance = new NewsItemCollection()); } 
        }

        private NewsItemCollection() { }

        public async void GetAllNewsItem(Action<ObservableCollection<NewsItem>> callback)
        {

           
                string basePage = await Downloader.GetStuffFromWeb("http://www.prothom-alo.com/roshalo");

                if (!string.IsNullOrEmpty(basePage))
                {

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(basePage);


                    try
                    {
                        callback(HtmlParser.ParseNewsFeedNodes(doc));
                    }
                    catch(Exception ex)
                    {
                        MessageBox.Show("Parsing Failed due to website issues, Kindly try later");
                    }
                }
            
                
               
           
            ProgressIndicatorHelper.HideProgressIndicator();
        }
            


    }
}
