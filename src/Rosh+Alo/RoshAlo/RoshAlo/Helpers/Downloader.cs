﻿using RoshAlo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RoshAlo.Helpers
{
    internal static class Downloader
    {
        private static HttpClient client;
        public static async Task<string> GetStuffFromWeb(string DamnUrl)
        {
            try
            {
                client = new HttpClient();
                
                var result = await client.GetStringAsync(new Uri(DamnUrl));
                return result;
            }
            catch
            {
                if ((App.Current.Resources["Locator"] as ViewModelLocator).Main.BusyIndicatorVisibility == false)
                {
                    (App.Current.Resources["Locator"] as ViewModelLocator).Main.NetworkNotFoundErrorVisible = true;
                }
                ProgressIndicatorHelper.HideProgressIndicator();
                return string.Empty;
            }
        }

        public static void CancelStuffFromWeb()
        {
            try
            {
                if (client != null)
                {
                    client.CancelPendingRequests();
                    ProgressIndicatorHelper.HideProgressIndicator();
                    (App.Current.Resources["Locator"] as ViewModelLocator).Main.BusyIndicatorVisibility = false;
                }
                
            }
            catch
            {}
        }

       


    }
}
