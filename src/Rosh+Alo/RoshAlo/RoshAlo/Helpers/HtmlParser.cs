﻿using HtmlAgilityPack;
using RoshAlo.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace RoshAlo.Helpers
{
    internal static class HtmlParser
    {
        public static ObservableCollection<NewsItem> ParseNewsFeedNodes(HtmlDocument doc)
        {
           
            //have to be inside a try catch
            HtmlNodeCollection AllNewsItemNodes = doc.DocumentNode.SelectNodes("//div[@class='each_news mb20']");
            ObservableCollection<NewsItem> ReturnList = new ObservableCollection<NewsItem>();

            foreach (var item in AllNewsItemNodes)
            {
                HtmlNode TitleNode = null;
                HtmlNode DateNode = null;
                HtmlNode SummaryNode = null;
                HtmlNode LinkNode = null;
                try
                {
                    TitleNode = item.SelectNodes(".//h2[@class='title']").FirstOrDefault();
                     DateNode = item.SelectNodes(".//div[@class='additional_info mb10']//span").LastOrDefault();
                    SummaryNode = item.SelectNodes(".//div[@class='content' or @class='content oh mb10']").FirstOrDefault();
                    LinkNode = TitleNode.SelectNodes(".//a//@href").FirstOrDefault();
                }
                catch { }
                HtmlNode AuthorNode = null;
                HtmlNode ImageNode=null;


                try { ImageNode = item.SelectNodes(".//img//@src").FirstOrDefault(); }
                catch { }

                try { AuthorNode = item.SelectNodes(".//div[@class='additional_info mb10']//span[@class='author']").FirstOrDefault(); }
                catch { }

                ReturnList.Add(new NewsItem() 
                {
                    title=TitleNode==null? string.Empty: HttpUtility.HtmlDecode(TitleNode.InnerText.Trim()),
                    summary= SummaryNode==null? string.Empty: HttpUtility.HtmlDecode(SummaryNode.InnerText.Trim()),
                    date= DateNode==null? string.Empty: HttpUtility.HtmlDecode(DateNode.InnerText.Trim()),
                    imagrUrl=ImageNode==null?string.Empty:"http:"+ImageNode.Attributes["src"].Value.Trim(),
                    link= LinkNode==null? string.Empty:LinkNode.Attributes["href"].Value,
                    author = AuthorNode == null ? string.Empty : HttpUtility.HtmlDecode(AuthorNode.InnerText.Trim())
                });
                           
            }

            return ReturnList;
        }

        public static NewsItem ParseDescription(string Html, NewsItem SrcItem)
        {
            try
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(Html);

                HtmlNodeCollection Paragraphs = doc.DocumentNode.SelectNodes("//div[@class='detail_widget widget']//div[@class='jw_detail_content_holder content pb10 oh']//p");
                SrcItem.details = (from para in Paragraphs
                                   select HttpUtility.HtmlDecode(para.InnerText).Trim()).ToList<string>();

                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Parsing failed due to  website, kindly try later");
            }

            return SrcItem;
        }
    }
}
