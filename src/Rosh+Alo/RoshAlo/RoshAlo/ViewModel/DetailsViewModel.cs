﻿using GalaSoft.MvvmLight;
using RoshAlo.Model;

namespace RoshAlo.ViewModel
{
    
    public class DetailsViewModel : ViewModelBase
    {
        
        
        public const string NewsItemContextPropertyName = "NewsItemContext";

        private NewsItem _NewsItemContext;
       
        public NewsItem NewsItemContext
        {
            get
            {
                return _NewsItemContext;
            }

            set
            {
                if (_NewsItemContext == value)
                {
                    return;
                }

                RaisePropertyChanging(NewsItemContextPropertyName);
                _NewsItemContext = value;
                RaisePropertyChanged(NewsItemContextPropertyName);
            }
        }

        public DetailsViewModel()
        {
        }
    }
}