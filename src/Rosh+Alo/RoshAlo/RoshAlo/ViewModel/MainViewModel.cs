﻿using GalaSoft.MvvmLight;
using RoshAlo.Model;
using System.Windows;
using System.Collections;
using System.Collections.ObjectModel;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using System;
using RoshAlo.Helpers;
using System.IO.IsolatedStorage;
using Coding4Fun.Toolkit.Controls;
using Microsoft.Phone.Tasks;

namespace RoshAlo.ViewModel
{

    public class MainViewModel : ViewModelBase
    {
        private NewsItemCollection _NewsItemCollection = NewsItemCollection.Instance;
        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;

        public const string AllNewsItemPropertyName = "AllNewsItem";
        private ObservableCollection<NewsItem> _AllNewsItem = null;
        public ObservableCollection<NewsItem> AllNewsItem
        {
            get
            {
                return _AllNewsItem;
            }

            set
            {
                if (_AllNewsItem == value)
                {
                    return;
                }

                RaisePropertyChanging(AllNewsItemPropertyName);
                _AllNewsItem = value;
                RaisePropertyChanged(AllNewsItemPropertyName);
            }
        }

        private bool _BusyIndicatorVisibility = false;
        public bool BusyIndicatorVisibility 
        {
            get { return _BusyIndicatorVisibility; }
            set { _BusyIndicatorVisibility = value; RaisePropertyChanged("BusyIndicatorVisibility"); }
        }

        private bool _NetworkNotFoundErrorVisible = false;
        public bool NetworkNotFoundErrorVisible { get { return _NetworkNotFoundErrorVisible; } set { _NetworkNotFoundErrorVisible = value; RaisePropertyChanged("NetworkNotFoundErrorVisible"); } }

        public RelayCommand<object> GetDetailsCommand { get; private set; }
        public RelayCommand RefreshPage { get; private set; }
        public RelayCommand ShowAbout { get; private set; }
        public RelayCommand<object> ShareNewsCommand { get; private set; }

        public MainViewModel()
        {
            GetDetailsCommand = new RelayCommand<object>((param) => ShowDetailsAction(param));
            RefreshPage = new RelayCommand(() => RefreshPageAction());
            ShowAbout = new RelayCommand(() => ShowAboutAction());
            ShareNewsCommand = new RelayCommand<object>((param) => ShareNewsAction(param));
          


            LoadPreviousData();
            LoadData();


        }

        private void ShareNewsAction(object param)
        {
            NewsItem SelectedItem = param as NewsItem;

            ShareLinkTask ShareTask = new ShareLinkTask();
            ShareTask.Title = SelectedItem.title;
            ShareTask.Message = SelectedItem.summary;
            ShareTask.LinkUri = new Uri(SelectedItem.link);
            ShareTask.Show();
        }

        private object ShowAboutAction()
        {
            AboutPrompt About = new AboutPrompt();
            About.Title = "আমাদের কথা";
            About.Body = "এই অ্যাপের সমস্ত ডাটা নেয়া হয়েছে \n http://www.prothom-alo.com/roshalo/ \nথেকে আর আপনাদের জন্য তা নিয়ে এসেছে \nনার্ডক্যাটস। ভালো লাগলে আমাদের জানান \nwww.facebook.com/nerdcats এ।";

            About.Show();

            return null;
        }

        private object LoadData()
        {
            try
            {
                _NewsItemCollection.GetAllNewsItem((x) => { AllNewsItem = x; });
            }
            catch
            {
                NetworkNotFoundErrorVisible = true;
            }

            return null;
        }

        private object RefreshPageAction()
        {
            ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingData);
            return LoadData();
        }

        private void LoadPreviousData()
        {
            if (_settings.Contains("lfeed"))
                AllNewsItem = _settings["lfeed"] as ObservableCollection<NewsItem>;
        }

        private async void ShowDetailsAction(object obj)
        {
            BusyIndicatorVisibility = true;
            NewsItem SelectedData = obj as NewsItem;

            //Need some stuff to Download here

            try
            {


                if (SelectedData.details != null && SelectedData.details.Count == 0)
                {
                    var result = await Downloader.GetStuffFromWeb(SelectedData.link);
                    if (!string.IsNullOrEmpty(result))
                    {
                        SelectedData = HtmlParser.ParseDescription(result, SelectedData);
                        AssignDetailsDataContextAndNavigate(SelectedData);
                    }
                    else
                    {
                        BusyIndicatorVisibility = false;
                    }
                }
                else
                {

                    AssignDetailsDataContextAndNavigate(SelectedData);
                }
            }
            catch
            {
                MessageBox.Show("Details downloading failed. Kindly try later");
            }
            
            
        }

        private void AssignDetailsDataContextAndNavigate(NewsItem SelectedData)
        {
            (App.Current.Resources["Locator"] as ViewModelLocator).Details.NewsItemContext = SelectedData;
            BusyIndicatorVisibility = false;

            Uri uri = new Uri("/DetailsPage.xaml", UriKind.Relative);
            Messenger.Default.Send<Uri>(uri, "Navigate");
        }

    }
}