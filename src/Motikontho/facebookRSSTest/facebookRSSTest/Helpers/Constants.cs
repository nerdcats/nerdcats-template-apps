﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace facebookRSSTest.Helpers
{
    public class Constants
    {
        public  const int MaxLength = 3500;
        public const int TitleLength = 70;
        public static readonly string LoadingData = "সম্ভাব্য হরতালের খবর খুঁজছি";
    }
}
