﻿using System;
using System.Net;
using System.Windows;
using System.Xml;
using Microsoft.Phone.Controls;

using System.Xml.Linq;
using System.Linq;
using facebookRSSTest;
using System.Text.RegularExpressions;
using facebookRSSTest.Helpers;
using GalaSoft.MvvmLight.Messaging;
using System.IO.IsolatedStorage;
using facebookRSSTest.ViewModel;
using System.Windows.Threading;
using System.Diagnostics;

namespace ReadFacebookPageRSS
{
    public partial class MainPage : PhoneApplicationPage
    {
        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;
        // Constructor
        public MainPage()
        {
          InitializeComponent();
          this.Loaded += MainPage_Loaded;
           
          Messenger.Default.Register<Uri>(this, "Navigate",(uri) => NavigationService.Navigate(uri));

        }

        void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            
            
        }

        // RSS url with the desired page ID
        
        private WebClient client = new WebClient();
        

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingData);
            if (HortalList.Items.Count > 0)
                ProgressIndicatorHelper.HideProgressIndicator();
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            SaveData();
            base.OnBackKeyPress(e);
        }

        private void SaveData()
        {
            if (_settings.Contains("lfeed"))
            {
                _settings["lfeed"] = (App.Current.Resources["Locator"] as ViewModelLocator).Main.FeedList;
            }
            else
            {
                _settings.Add("lfeed", (App.Current.Resources["Locator"] as ViewModelLocator).Main.FeedList);
            }

            _settings.Save();
        }

        private void AdControl_AdClick(object sender, AdDuplex.AdClickEventArgs e)
        {
            AdControl.Visibility = Visibility.Collapsed;
        }

        private void AdControl_AdLoaded(object sender, AdDuplex.AdLoadedEventArgs e)
        {
          
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(80);
            timer.Tick += timer_Tick;
            timer.Start();
        }

        void timer_Tick(object sender, EventArgs e)
        {
            AdControl.Visibility = Visibility.Collapsed;
            (sender as DispatcherTimer).Stop();
            
            
        }


       


        

        // Http web request responcr handler

    }
}