﻿using facebookRSSTest.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using System.Xml.Linq;

namespace facebookRSSTest.Helpers
{
    internal static class WebHelper
    {
        
        public static async void GetRss(string url, Action<List<FeedData>> callback, bool IsBangla=true)
        {
            try
            {
                HttpClient client = new HttpClient();
                var result = await client.GetStringAsync(new Uri(url));
                callback(ParseRss(result, IsBangla));
                ProgressIndicatorHelper.HideProgressIndicator();
            }
            catch
            {
                (App.Current.Resources["Locator"] as ViewModelLocator).Main.NetworkNotFoundErrorVisible = true;
                ProgressIndicatorHelper.HideProgressIndicator();
            }
        }

        private static List<FeedData> ParseRss(string p , bool isBangla)
        {
            
            XElement doc = XElement.Parse(p);

            var items = doc.Descendants("item");
            return (from item in items
                   select new FeedData()
                   {
                       Image=FetchLinksFromSource(item.Element("description").Value.Trim()),
                       
                       Title = isBangla ? ReturnTitle(SanitizeText(item.Element("description").Value)) : item.Element("title").Value.Trim(),
                       Details = SanitizeText(item.Element("description").Value),
                       Link = item.Element("link").Value,
                       Date = item.Element("pubDate").Value.Substring(0,16),
                       openBrowser = (string.IsNullOrEmpty(SanitizeText(item.Element("description").Value).Trim()) || SanitizeText(item.Element("description").Value).Length > Constants.MaxLength) ? true : false
                   }).ToList<FeedData>();
           
        }

        private static string ReturnTitle(string p)
        {
            if (p.Length > Constants.TitleLength)
                return p.Substring(0, Constants.TitleLength) + "...";
            else
                return p;
        }

        private static string SanitizeText(string value)
        {
            string fixedString;
            fixedString = Regex.Replace(value.ToString(), "<[^>]+>", string.Empty);

            // Remove encoded HTML characters
            fixedString = HttpUtility.HtmlDecode(fixedString);

            return fixedString;
        }

        private static string FetchLinksFromSource(string htmlSource)
        {
            List<string> links = new List<string>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                links.Add(href);
            }
            return links.Count > 0 ? links.First() : null;
        }

        
    }
}
