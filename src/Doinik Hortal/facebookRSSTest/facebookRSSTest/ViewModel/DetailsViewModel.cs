﻿using GalaSoft.MvvmLight;

namespace facebookRSSTest.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class DetailsViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the DetailsViewModel class.
        /// </summary>
        /// 
        /// <summary>
        /// The <see cref="FeedDataContext" /> property's name.
        /// </summary>
        public const string FeedDataContextPropertyName = "FeedDataContext";

        private FeedData _FeedDataContext ;

        /// <summanu
        /// Sets and gets the FeedDataContext property.
        /// Changes to that property's value raise the PropertyChanged event. 
        /// </summary>
        public FeedData FeedDataContext
        {
            get
            {
                return _FeedDataContext;
            }

            set
            {
                if (_FeedDataContext == value)
                {
                    return;
                }

                RaisePropertyChanging(FeedDataContextPropertyName);
                _FeedDataContext = value;
                RaisePropertyChanged(FeedDataContextPropertyName);
            }
        }

        public DetailsViewModel()
        {
            
        }
    }
}