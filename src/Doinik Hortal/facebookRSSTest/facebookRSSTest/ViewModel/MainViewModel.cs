﻿using Coding4Fun.Toolkit.Controls;
using facebookRSSTest.Helpers;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using Microsoft.Phone.Tasks;
using System;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Windows;

namespace facebookRSSTest.ViewModel
{
    
    public class MainViewModel : ViewModelBase
    {
       
        private string RssUrl = "https://www.facebook.com/feeds/page.php?format=rss20&id=1379644332283538";

        private IsolatedStorageSettings _settings = IsolatedStorageSettings.ApplicationSettings;

        private ObservableCollection<FeedData> _FeedList;
        public ObservableCollection<FeedData> FeedList 
        {
            get { return _FeedList; }
            set { _FeedList = value; RaisePropertyChanged("FeedList"); }
        }

        private bool _NetworkNotFoundErrorVisible = false;
        public bool NetworkNotFoundErrorVisible { get { return _NetworkNotFoundErrorVisible; } set { _NetworkNotFoundErrorVisible = value; RaisePropertyChanged("NetworkNotFoundErrorVisible"); } }

        public RelayCommand<object> ListItemSelectedCommand { get; private set; }
        public RelayCommand RefreshPage { get; private set; }
        public RelayCommand ShowAbout { get; private set; }



        public MainViewModel()
        {
            ListItemSelectedCommand = new RelayCommand<object>((param)=>ShowDetailsAction(param));
            RefreshPage = new RelayCommand(()=>RefreshPageAction());
            ShowAbout = new RelayCommand(()=>ShowAboutAction());
            LoadPreviousData();
            LoadData();
        }

        private object ShowAboutAction()
        {
            AboutPrompt About = new AboutPrompt();
            About.Title = "আমাদের কথা";
            About.Body = "এই অ্যাপের সমস্ত ডাটা নেয়া হয়েছে \nহরতাল Updates ফেসবুক পেজ \n(www.facebook.com/updatesbd09)\nথেকে আর আপনাদের জন্য তা নিয়ে এসেছে \nনার্ডক্যাটস। ভালো লাগলে আমাদের জানান \nwww.facebook.com/nerdcats এ।";
           
            About.Show();

            return null;
        }

        private void LoadPreviousData()
        {
            if (_settings.Contains("lfeed"))
                FeedList = _settings["lfeed"] as ObservableCollection<FeedData>;
        }

        private object RefreshPageAction()
        {
            ProgressIndicatorHelper.ShowProgressIndicator(Constants.LoadingData);
            return LoadData();
        }

        private object LoadData()
        {
            try
            {
                WebHelper.GetRss(RssUrl, AssignToListBox);
                NetworkNotFoundErrorVisible = false;
            }
            catch
            { NetworkNotFoundErrorVisible = true; }

            return null;
        }

        public void ShowDetailsAction(object obj)
        {
            FeedData SelectedData = obj as FeedData;
            (App.Current.Resources["Locator"] as ViewModelLocator).Details.FeedDataContext = SelectedData;

            if (SelectedData.openBrowser || SelectedData.Details.Length>Constants.MaxLength)
            {
                WebBrowserTask webBrowserTask = new WebBrowserTask();
                webBrowserTask.Uri = new Uri(SelectedData.Link);
                webBrowserTask.Show();

            }
            else
            {
                Uri uri = new Uri("/DetailsView.xaml", UriKind.Relative);
                Messenger.Default.Send<Uri>(uri, "Navigate");
            }


            
        }

        

        private void AssignToListBox(System.Collections.Generic.List<FeedData> ParsedData)
        {
            FeedList = new ObservableCollection<FeedData>(ParsedData);
        }


    }
}