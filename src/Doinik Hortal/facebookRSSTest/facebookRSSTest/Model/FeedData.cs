﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace facebookRSSTest
{
    public class FeedData
    {
        public string Title { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public string Details { get; set; }
        public bool openBrowser { get; set; }
        public string Date { get; set; }

        
    }
}
